
UI Elements
============================================

Templates
------------------

Panel
^^^^^^^^^^^^^^^^^^
.. automodule:: scripts.ui_elements.templates.panel
    :members:

Skill Container
^^^^^^^^^^^^^^^^^^
.. automodule:: scripts.ui_elements.templates.skill_container
    :members:



Colours
------------------
.. automodule:: scripts.ui_elements.colours
    :members:

Entity Info
------------------
.. automodule:: scripts.ui_elements.entity_info
    :members:

Message Log
------------------
.. automodule:: scripts.ui_elements.message_log
    :members:

Palette
------------------
.. automodule:: scripts.ui_elements.palette
    :members:

Skill Bar
------------------
.. automodule:: scripts.ui_elements.skill_bar
    :members:

Targeting Overlay
------------------
.. automodule:: scripts.ui_elements.targeting_overlay
    :members:







