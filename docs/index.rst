.. NotQuiteParadise documentation master file, created by
   sphinx-quickstart on Mon Jun 24 20:06:50 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Not Quite Paradise's documentation!
==================================================
Last updated on |today| for v|version|.

..  toctree::
    :maxdepth: 1

    core
    dev_tools
    event_handlers
    events
    managers
    skills
    ui
    world



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
