
Skills
============================================

Affliction
------------------
.. automodule:: scripts.skills.affliction
    :show-inheritance:

Effect
------------------
.. automodule:: scripts.skills.effect
    :show-inheritance:

Skill
------------------
.. automodule:: scripts.skills.skill
    :show-inheritance:








