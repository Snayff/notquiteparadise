
Events
====================

Entity
------------------
.. automodule:: scripts.events.entity_events
    :show-inheritance:

Game
------------------
.. automodule:: scripts.events.game_events
    :show-inheritance:

Map
------------------
.. automodule:: scripts.events.map_events
    :show-inheritance:

UI
------------------
.. automodule:: scripts.events.ui_events
    :show-inheritance:






