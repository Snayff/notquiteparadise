
Event Handlers
============================================

Entity
------------------
.. automodule:: scripts.event_handlers.entity_handler
    :show-inheritance:

Game
------------------
.. automodule:: scripts.event_handlers.game_handler
    :show-inheritance:

God
------------------
.. automodule:: scripts.event_handlers.god_handler
    :show-inheritance:

Map
------------------
.. automodule:: scripts.event_handlers.map_handler
    :show-inheritance:

UI
------------------
.. automodule:: scripts.event_handlers.ui_handler
    :show-inheritance:






