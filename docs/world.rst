
World
============================================

Combat Stats
------------------
.. automodule:: scripts.world.combat_stats
    :show-inheritance:

Components
------------------
.. automodule:: scripts.world.components
    :show-inheritance:

Game Map
------------------
.. automodule:: scripts.world.game_map
    :show-inheritance:

Tile
------------------
.. automodule:: scripts.world.tile
    :show-inheritance: