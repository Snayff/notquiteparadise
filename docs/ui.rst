
UI
============================================

Basic
------------------

Colours
^^^^^^^^^^^^^^^^^^
.. automodule:: scripts.ui.basic.colours
    :show-inheritance:

Fonts
^^^^^^^^^^^^^^^^^^
.. automodule:: scripts.ui.basic.fonts
    :show-inheritance:

Palette
^^^^^^^^^^^^^^^^^^
.. automodule:: scripts.ui.basic.palette
    :show-inheritance:

UI Elements
------------------

Camera
^^^^^^^^^^^^^^^^^^
.. automodule:: scripts.ui.ui_elements.camera
    :show-inheritance:

Entity Info
^^^^^^^^^^^^^^^^^^
.. automodule:: scripts.ui.ui_elements.entity_info
    :show-inheritance:

Message Log
^^^^^^^^^^^^^^^^^^
.. automodule:: scripts.ui.ui_elements.message_log
    :show-inheritance:

Screen Message
^^^^^^^^^^^^^^^^^^
.. automodule:: scripts.ui.ui_elements.screen_message
    :show-inheritance:

Skill Bar
^^^^^^^^^^^^^^^^^^
.. automodule:: scripts.ui.ui_elements.skill_bar
    :show-inheritance: